# janus

По задумке, Янус - это несколько независимых игр являющихся единым целым. Не в смысле смешения жанров или сборников игр 1000 в 1. А в том смысле, что из одной и той же кодовой базы будет появляться и развиваться отдельные независимые игры

Например, если создать бой в стиле героев, то это можно дальше развить в абсолютно независимые игры:

- Последовательность боев нарастающей сложности (Пример: `Ash of Gods`)
- Спортивный менеджер (Пример: `Blood Bowl`)
- 4D-стратегия (Примеры: `Planetfall`, `Endless Legend`)
- Классические герои (Пример: `Heroes 3`)
- Классический Kings Bounty (Пример: `Kings Bounty`)
- и т.д.

А можно все те же игры сделать на основе боёв из `Disciples` или любой другой механики

Переключаясь между разными ветками проекта получаем абсолютно разные игры!
